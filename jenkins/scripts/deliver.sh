#!/usr/bin/env sh
echo 'Building your docker image'
set -x
npm run build
echo 'Build complete'
npm run deploy
set +x

echo 'Now...'
echo 'Visit http://localhost:3000 to see your Node.js application in action.'
echo 'created your initial Pipeline as a Jenkinsfile.)'